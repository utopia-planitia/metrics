#!/bin/bash
set -euxo pipefail

curl --silent --fail http://kube-prometheus-stack-prometheus:9090/status

[[ $( curl --silent --fail http://kube-prometheus-stack-prometheus:9090/api/v1/query\?query=node_filesystem_free_bytes%7Bmountpoint%3D%22%2F%22%7D | jq '.data.result | length') -gt 0 ]]
