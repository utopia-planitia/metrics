apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  annotations:
    kubernetes.io/ingress.class: nginx
    nginx.ingress.kubernetes.io/auth-realm: Authentication Required
    nginx.ingress.kubernetes.io/auth-secret: basic-auth
    nginx.ingress.kubernetes.io/auth-type: basic
  name: prometheus
spec:
  rules:
    - host: prometheus.{{ .Values.clusterDomain }}
      http:
        paths:
          - backend:
              service:
                name: kube-prometheus-stack-prometheus
                port:
                  number: 9090
            path: /
            pathType: ImplementationSpecific
  tls:
    - hosts:
        - prometheus.{{ .Values.clusterDomain }}
      secretName: "{{ .Values.tlsSecretName }}"
