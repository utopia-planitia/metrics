apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  annotations:
    kubernetes.io/ingress.class: nginx
    nginx.ingress.kubernetes.io/auth-realm: Authentication Required
    nginx.ingress.kubernetes.io/auth-secret: basic-auth
    nginx.ingress.kubernetes.io/auth-type: basic
  name: jaeger
spec:
  rules:
    - host: jaeger.{{ .Values.clusterDomain }}
      http:
        paths:
          - backend:
              service:
                name: jaeger
                port:
                  number: 16686
            path: /
            pathType: ImplementationSpecific
  tls:
    - hosts:
        - jaeger.{{ .Values.clusterDomain }}
      secretName: "{{ .Values.tlsSecretName }}"
