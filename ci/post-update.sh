#!/bin/bash
set -exuo pipefail

CHART_VERSION=$(yq -r '.releases[] | select( .chart == "prometheus-community/kube-prometheus-stack" ) | .version' kube-prometheus-stack/helmfile.yaml | sort | uniq)

echo "CHART_VERSION: ${CHART_VERSION:?}"

mkdir -p kube-prometheus-stack-crds/chart/templates
rm -f kube-prometheus-stack-crds/chart/templates/*

curl -fsSL -o kube-prometheus-stack-crds/chart/templates/crd-alertmanagerconfigs.yaml https://raw.githubusercontent.com/prometheus-community/helm-charts/kube-prometheus-stack-${CHART_VERSION}/charts/kube-prometheus-stack/charts/crds/crds/crd-alertmanagerconfigs.yaml
curl -fsSL -o kube-prometheus-stack-crds/chart/templates/crd-alertmanagers.yaml https://raw.githubusercontent.com/prometheus-community/helm-charts/kube-prometheus-stack-${CHART_VERSION}/charts/kube-prometheus-stack/charts/crds/crds/crd-alertmanagers.yaml
curl -fsSL -o kube-prometheus-stack-crds/chart/templates/crd-podmonitors.yaml https://raw.githubusercontent.com/prometheus-community/helm-charts/kube-prometheus-stack-${CHART_VERSION}/charts/kube-prometheus-stack/charts/crds/crds/crd-podmonitors.yaml
curl -fsSL -o kube-prometheus-stack-crds/chart/templates/crd-probes.yaml https://raw.githubusercontent.com/prometheus-community/helm-charts/kube-prometheus-stack-${CHART_VERSION}/charts/kube-prometheus-stack/charts/crds/crds/crd-probes.yaml
curl -fsSL -o kube-prometheus-stack-crds/chart/templates/crd-prometheuses.yaml https://raw.githubusercontent.com/prometheus-community/helm-charts/kube-prometheus-stack-${CHART_VERSION}/charts/kube-prometheus-stack/charts/crds/crds/crd-prometheuses.yaml
curl -fsSL -o kube-prometheus-stack-crds/chart/templates/crd-prometheusrules.yaml https://raw.githubusercontent.com/prometheus-community/helm-charts/kube-prometheus-stack-${CHART_VERSION}/charts/kube-prometheus-stack/charts/crds/crds/crd-prometheusrules.yaml
curl -fsSL -o kube-prometheus-stack-crds/chart/templates/crd-servicemonitors.yaml https://raw.githubusercontent.com/prometheus-community/helm-charts/kube-prometheus-stack-${CHART_VERSION}/charts/kube-prometheus-stack/charts/crds/crds/crd-servicemonitors.yaml
curl -fsSL -o kube-prometheus-stack-crds/chart/templates/crd-thanosrulers.yaml https://raw.githubusercontent.com/prometheus-community/helm-charts/kube-prometheus-stack-${CHART_VERSION}/charts/kube-prometheus-stack/charts/crds/crds/crd-thanosrulers.yaml

chart-prettier --stdin=false kube-prometheus-stack-crds/chart/templates
