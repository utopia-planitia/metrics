apiVersion: monitoring.coreos.com/v1
kind: PrometheusRule
metadata:
  labels:
    app: kube-prometheus-stack
    release: "kube-prometheus-stack"
  name: additional-metrics
  namespace: monitoring
spec:
  groups:
    - name: additional-metrics
      rules:
        - alert: IptablesRulesCountError
          expr: iptables_most_duplicated_rule_count > 5
          for: 10m
          labels:
            severity: warning
          annotations:
            message: |
              Too many duplicates of an iptables rule found: {{`{{ $value }}`}}
